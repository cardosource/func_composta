const botaoCOMPRA = document.querySelector('.livro-b-box-adicionar')
const icone = document.querySelector('.fa-cart-shopping')
const menos = document.querySelector('.livro-b-box-quantia-menos')
const mais = document.querySelector('.livro-b-box-quantia-mais')
var quantia =  document.getElementById('quantidadeLivro')


function compra(campoMenor,campoMaior,total){
    return function(valor){
        return campoMaior(campoMenor(total(valor)))
    }
}

function maior(){
    mais.addEventListener('click', function(){
       let quantidadeAtual = Number(quantia.innerHTML)
       quantidadeAtual += 1
       quantia.innerHTML = quantidadeAtual;
    })
}


function menor(){
    menos.addEventListener('click', function(){
       let quantidadeAtual = Number(quantia.innerHTML)
       if(quantidadeAtual > 0){
       quantidadeAtual -= 1
       quantia.innerHTML = quantidadeAtual;
       }
    })
}
function adicionado(){
    
    if(Number(quantia.innerHTML)  > 0){    
        icone.style.marginTop = '20px'
        icone.style.fontSize = '30px'
        setTimeout(()=>{icone.style.fontSize = '40px';
                        icone.style.marginTop = '10px'},700)
        
    }
}

function quantidadeDElivos(campoMenor,campoMaior){
    return function(valor){
        return campoMaior(campoMenor(total(valor)))
    }
}


quantidadeDElivos(
    menor(),
    maior()

)
botaoCOMPRA.addEventListener('click', function(event) {    
    adicionado()

    });