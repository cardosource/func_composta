
# Aplicando função composta no projeto

Neste projeto foi implementado funções compostas para aprimorar a experiência do usuário ao interagir com elementos como botões de adicionar e remover itens do carrinho de compras.


Uma função composta  é uma função que é criada combinando ou encadeando várias funções individuais para realizar uma série de operações relacionadas.


## Função composta

```javascript
function quantidadeDElivos(campoMenor,campoMaior){
    return function(valor){
        return campoMaior(campoMenor(total(valor)))
    }
}


quantidadeDElivos(
    menor(),
    maior()

)

```

